'use strict';
const lodash = require('lodash');
const isExecutionHalt = (instruction) => {
  return ['JUMP', 'RETURN', 'STOP', 'REVERT', 'INVALID'].includes(instruction);
};

const padToBytes2 = (hex) => {
  if (hex.substr(0, 2) === '0x') hex = hex.substr(2);
  return '0x' + '0'.repeat(4 - Math.min(4, hex.length)) + hex;
};

const strip = (instructions) => {
  instructions.forEach((v) => {
    if (v[0].match('PUSH')) v.splice(1, 1);
    else v.splice(1, 2);
  });
};

const stripReachable = (jumpdests) => {
  const dests = lodash.difference(Object.keys(jumpdests), ['unreachable']);
  dests.forEach((v) => {
    strip(jumpdests[v]);
  });
  return jumpdests;
};
  

Object.assign(module.exports, {
  isExecutionHalt,
  stripReachable,
  padToBytes2,
  strip
});
