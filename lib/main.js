'use strict';

const util = require('.//util');
const solidity = require('./solidity');
const segment = require('./segment');
const annotate = require('./annotate');
const dis = require('./dis');

const disassemble = (bytecode) => {
  return annotate.annotate(util.stripReachable(solidity.decorateJumps(segment.segment(dis(bytecode)))));
};

module.exports = disassemble;
