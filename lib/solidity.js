'use strict';

const util = require('./util');
const lodash = require('lodash');

const decorateJumps = (segmented) => {
  const dests = lodash.difference(Object.keys(segmented), ['unreachable']);
  dests.forEach((jumpdest) => {
    segmented[jumpdest].forEach(([ instruction, byte, data ], i, ary) => {
      if (instruction === 'JUMPI') {
        const dest = util.padToBytes2(ary[i - 1][2]);
        ary[i].jumpdest = dest;
      }
    });
  });
  return segmented;
};

module.exports.decorateJumps = decorateJumps;
